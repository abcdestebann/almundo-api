import * as firebase from 'firebase-admin'
import config from '../config/main'

/**
 * @export
 * @class Hotel
 */
export default class Hotel {

   // VARIABLES
   private _database: firebase.firestore.Firestore

   /**
    * CONSTRUCTOR
    * Creates an instance of Hotel.
    * @param {firebase.firestore.Firestore} database 
    */
   constructor(database: firebase.firestore.Firestore) {
      this._database = database
   }

   // METHODS

   /**
    * This method create a new Hotel in database returning a promise
    * @param {any} hotel 
    * @returns {Promise<any>} 
    */
   async createHotel(hotel: any): Promise<any> {
      const result = await this._database.collection(config.database.COLLECTIONHOTELS).add(hotel)
      return result
   }

   /**
    * This method consult to the database to get a specific hotel by id that receive as parameter
    * @param {(number | string)} id 
    * @returns {Promise<any>} 
    */
   async getHotel(id: number | string): Promise<any> {
      const result = await this._database.collection(config.database.COLLECTIONHOTELS).doc(String(id)).get()
      return result
   }

   /**
    * This method consult to the database to get all records of hotels
    * @returns {Promise<any>} 
    */
   async getAllHotels(): Promise<any> {
      const result = await this._database.collection(config.database.COLLECTIONHOTELS).get()
      return result
   }

   /**
    * This method update one record hotel in the database using the specific id
    * @param {(number | string)} id 
    * @param {*} hotel 
    * @returns {Promise<any>}
    */
   async updateHotel(id: number | string, hotel: any): Promise<any> {
      const result = await this._database.collection(config.database.COLLECTIONHOTELS).doc(String(id)).update(hotel)
      return result
   }

   /**
    * This method delete one record of Hotel in the database using the specific id
    * @param {(number | string)} id 
    * @returns {Promise<any>} 
    */
   async deleteHotel(id: number | string): Promise<any> {
      const result = await this._database.collection(config.database.COLLECTIONHOTELS).doc(String(id)).delete()
      return result
   }

}