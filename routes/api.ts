import * as express from 'express'

import {
   createHotel,
   getHotel,
   getAllHotels,
   updateHotel,
   deleteHotel
} from '../controllers/HotelController'; // Import Hotel Cotroller to use its functions

export default (app) => {

   const apiRoutes = express.Router(); // Create main route
   app.use('/api', apiRoutes)

   const hotelsRoutes = express.Router(); // Create sub route
   apiRoutes.use('/hotels', hotelsRoutes)

   // HOTELS ROUTES
   hotelsRoutes.post('/createHotel', createHotel)
   hotelsRoutes.post('/getHotel', getHotel)
   hotelsRoutes.get('/', getAllHotels)
   hotelsRoutes.put('/updateHotel', updateHotel)
   hotelsRoutes.delete('/deleteHotel', deleteHotel)
}