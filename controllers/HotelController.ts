import { NextFunction, Request, Response } from 'express'
import * as firebase from 'firebase-admin'

import Hotel from '../models/HotelModel'
import config from '../config/main'


verifyFirebaseApp()

const database = firebase.firestore();

/**
 * This method verify the app initialized of firebase to use the app or initialize again the app
 */
function verifyFirebaseApp() {
	if (!firebase.apps.length) {
		firebase.initializeApp({ credential: firebase.credential.cert(<firebase.ServiceAccount>config.database.serviceAccount) })
	} else {
		firebase.app();
	}
}

/**
 * This method instance a Hotel to create a record in database and response if creating was success or declined
 * @export
 * @param {Request} req 
 * @param {Response} res 
 * @param {NextFunction} next 
 */
export function createHotel(req: Request, res: Response, next: NextFunction) {
	const dataHotel: any = req.body.hotel
	const hotel: Hotel = new Hotel(database)
	hotel.createHotel(dataHotel)
		.then((snap) => res.status(200).json({ data: snap }))
		.catch((error) => res.status(500).json({ error }))
}

/**
 * This method instance a Hotel to get a record in database and response if was success or declined
 * @export
 * @param {Request} req 
 * @param {Response} res 
 * @param {NextFunction} next 
 */
export function getHotel(req: Request, res: Response, next: NextFunction) {
	const idHotel: any = req.body.id
	const hotel: Hotel = new Hotel(database)
	hotel.getHotel(idHotel)
		.then((snap: any) => res.status(200).json({ hotel: snap.data() }))
		.catch((error) => res.status(500).json({ error }))
}

/**
 * This method instance a Hotel to get all records in database and response if was success or declined
 * @export
 * @param {Request} req 
 * @param {Response} res 
 * @param {NextFunction} next 
 */
export function getAllHotels(req: Request, res: Response, next: NextFunction) {
	const hotels: Array<any> = []
	const hotel: Hotel = new Hotel(database)
	hotel.getAllHotels()
		.then((snap) => {
			snap.forEach((doc) => hotels.push(doc.data()));
			res.status(200).json({ hotels });
		})
		.catch((error) => res.status(500).json({ error }))
}

/**
 * This method instance a Hotel to update a record in database and response if was success or declined 
 * @export
 * @param {Request} req 
 * @param {Response} res 
 * @param {NextFunction} next 
 */
export function updateHotel(req: Request, res: Response, next: NextFunction) {
	const idHotel: any = req.body.id
	const dataHotel: any = req.body.hotel
	const hotel: Hotel = new Hotel(database)
	hotel.updateHotel(idHotel, dataHotel)
		.then((data) => res.status(200).json({ data }))
		.catch((error) => res.status(500).json({ error }))
}

/**
 * This method instance a Hotel to delete a record in database and response if was success or declined 
 * @export
 * @param {Request} req 
 * @param {Response} res 
 * @param {NextFunction} next 
 */
export function deleteHotel(req: Request, res: Response, next: NextFunction) {
	const idHotel: any = req.body.id
	const hotel: Hotel = new Hotel(database)
	hotel.deleteHotel(idHotel)
		.then((data) => res.status(200).json({ data }))
		.catch((error) => res.status(500).json({ error }))
}

