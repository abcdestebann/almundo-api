"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const firebase = require("firebase-admin");
const HotelModel_1 = require("../models/HotelModel");
const main_1 = require("../config/main");
verifyFirebaseApp();
const database = firebase.firestore();
/**
 * This method verify the app initialized of firebase to use the app or initialize again the app
 */
function verifyFirebaseApp() {
    if (!firebase.apps.length) {
        firebase.initializeApp({ credential: firebase.credential.cert(main_1.default.database.serviceAccount) });
    }
    else {
        firebase.app();
    }
}
/**
 * This method instance a Hotel to create a record in database and response if creating was success or declined
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
function createHotel(req, res, next) {
    const dataHotel = req.body.hotel;
    const hotel = new HotelModel_1.default(database);
    hotel.createHotel(dataHotel)
        .then((snap) => res.status(200).json({ data: snap }))
        .catch((error) => res.status(500).json({ error }));
}
exports.createHotel = createHotel;
/**
 * This method instance a Hotel to get a record in database and response if was success or declined
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
function getHotel(req, res, next) {
    const idHotel = req.body.id;
    const hotel = new HotelModel_1.default(database);
    hotel.getHotel(idHotel)
        .then((snap) => res.status(200).json({ hotel: snap.data() }))
        .catch((error) => res.status(500).json({ error }));
}
exports.getHotel = getHotel;
/**
 * This method instance a Hotel to get all records in database and response if was success or declined
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
function getAllHotels(req, res, next) {
    const hotels = [];
    const hotel = new HotelModel_1.default(database);
    hotel.getAllHotels()
        .then((snap) => {
        snap.forEach((doc) => hotels.push(doc.data()));
        res.status(200).json({ hotels });
    })
        .catch((error) => res.status(500).json({ error }));
}
exports.getAllHotels = getAllHotels;
/**
 * This method instance a Hotel to update a record in database and response if was success or declined
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
function updateHotel(req, res, next) {
    const idHotel = req.body.id;
    const dataHotel = req.body.hotel;
    const hotel = new HotelModel_1.default(database);
    hotel.updateHotel(idHotel, dataHotel)
        .then((data) => res.status(200).json({ data }))
        .catch((error) => res.status(500).json({ error }));
}
exports.updateHotel = updateHotel;
/**
 * This method instance a Hotel to delete a record in database and response if was success or declined
 * @export
 * @param {Request} req
 * @param {Response} res
 * @param {NextFunction} next
 */
function deleteHotel(req, res, next) {
    const idHotel = req.body.id;
    const hotel = new HotelModel_1.default(database);
    hotel.deleteHotel(idHotel)
        .then((data) => res.status(200).json({ data }))
        .catch((error) => res.status(500).json({ error }));
}
exports.deleteHotel = deleteHotel;
//# sourceMappingURL=HotelController.js.map