"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const main_1 = require("../config/main");
/**
 * @export
 * @class Hotel
 */
class Hotel {
    /**
     * CONSTRUCTOR
     * Creates an instance of Hotel.
     * @param {firebase.firestore.Firestore} database
     */
    constructor(database) {
        this._database = database;
    }
    // METHODS
    /**
     * This method create a new Hotel in database returning a promise
     * @param {any} hotel
     * @returns {Promise<any>}
     */
    createHotel(hotel) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield this._database.collection(main_1.default.database.COLLECTIONHOTELS).add(hotel);
            return result;
        });
    }
    /**
     * This method consult to the database to get a specific hotel by id that receive as parameter
     * @param {(number | string)} id
     * @returns {Promise<any>}
     */
    getHotel(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield this._database.collection(main_1.default.database.COLLECTIONHOTELS).doc(String(id)).get();
            return result;
        });
    }
    /**
     * This method consult to the database to get all records of hotels
     * @returns {Promise<any>}
     */
    getAllHotels() {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield this._database.collection(main_1.default.database.COLLECTIONHOTELS).get();
            return result;
        });
    }
    /**
     * This method update one record hotel in the database using the specific id
     * @param {(number | string)} id
     * @param {*} hotel
     * @returns {Promise<any>}
     */
    updateHotel(id, hotel) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield this._database.collection(main_1.default.database.COLLECTIONHOTELS).doc(String(id)).update(hotel);
            return result;
        });
    }
    /**
     * This method delete one record of Hotel in the database using the specific id
     * @param {(number | string)} id
     * @returns {Promise<any>}
     */
    deleteHotel(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield this._database.collection(main_1.default.database.COLLECTIONHOTELS).doc(String(id)).delete();
            return result;
        });
    }
}
exports.default = Hotel;
//# sourceMappingURL=HotelModel.js.map