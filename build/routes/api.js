"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express = require("express");
const HotelController_1 = require("../controllers/HotelController"); // Import Hotel Cotroller to use its functions
exports.default = (app) => {
    const apiRoutes = express.Router(); // Create main route
    app.use('/api', apiRoutes);
    const hotelsRoutes = express.Router(); // Create sub route
    apiRoutes.use('/hotels', hotelsRoutes);
    // HOTELS ROUTES
    hotelsRoutes.post('/createHotel', HotelController_1.createHotel);
    hotelsRoutes.post('/getHotel', HotelController_1.getHotel);
    hotelsRoutes.get('/', HotelController_1.getAllHotels);
    hotelsRoutes.put('/updateHotel', HotelController_1.updateHotel);
    hotelsRoutes.delete('/deleteHotel', HotelController_1.deleteHotel);
};
//# sourceMappingURL=api.js.map