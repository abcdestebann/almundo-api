"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// LIBRARIES
const express = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const logger = require("morgan");
const helmet = require("helmet");
const cors = require("cors");
const api_1 = require("./routes/api");
const main_1 = require("./config/main");
// Inicializar aplicacion de express
const app = express();
// Express Middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(logger('dev'));
app.use(helmet());
app.use(cors());
app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-Width, Content-Type, Accept, Access-Control-Allow-Request-Method');
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
    next();
});
// Inicializa Router
api_1.default(app);
// Inicializar Server
let server;
if (process.env.NODE_ENV !== main_1.default.test_env) {
    server = app.listen(main_1.default.port, () => {
        console.log(`Server Listening on port ${main_1.default.port}`);
    });
}
else {
    server = app.listen(main_1.default.test_port, () => {
        console.log(`Server Listening on port ${main_1.default.port}`);
    });
}
exports.default = server;
//# sourceMappingURL=index.js.map