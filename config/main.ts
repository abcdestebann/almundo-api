// This file sets all configurations that project needs

const config = {
   port: process.env.PORT || 3000, // Port
   database: { // Firebase config json
      COLLECTIONHOTELS: 'hotels',
      serviceAccount: {
         "type": "service_account",
         "project_id": "almundo-ar",
         "private_key_id": "f2646040814968225a975239457e9be97e6719b0",
         "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCzRziGrOqoB57d\nyuLgsKuOwoG5DUIU4cyfVsSCoZUX4xiaLaXvWM4RHZQxrzVkhbKmUc/HQt/3ueiK\nqf33yjyzjU176PmcIoUqFCNTs1sCQFvfReDxhutA4/MmXnHfx2o/ZFK3M6BmFXgT\n7OhZ4umD8h/K1iTsXSfUo7vcV35FcQDO7JvvRZhH8tM81pEYY8793+j89E8/6k/K\ngpQwgj4Iz9kEQAy78TCFOyIxqDPOKcM7uKKvvbQJpLOPFeKy8bLKN7c1zfG75xTw\nyj/fCYfW2+gw0VpMSPk+P3klhbdsd+fSEi5afnIhImSo5qeqqeoOxkOMve16Lat9\n7PaVxlRjAgMBAAECggEAKzQbJa1vIU4w1U9K7I320gECxQcyaRjfOHjvA3IQHYEP\nUTx19B+pDUvdBjosekyT+GvX0DPICImHRj9gIkJc7YI8bYN//8X75F+VAdtDr9+1\npDFRW7NBZ1JnqtPwylZivuMc4eRxn5hF5ztc5Sh/UNpdGgMQQ9vROPe6UofNOMty\n+mPhs3yfmBzkSfdYXAqsL+McBiSayL8OomkvhhnBGpZvJ1/0PeEPlsoulAoXiinY\nkrk5lhRoEw+EqNEmdZN5ocCb6GYvv6hLXgeHZAvhaGRxqWoaFtApGP5nI82qe60g\nHt9pO8tO6oyroesVQze8Ss6zB44j5VMIHQLUWG+66QKBgQDaE3cwxsTRRs5V+/CH\nVXeWzDJU8RWhmUkrq2wgTggS5JpLBpcnEzL4ZOFCjCDmY82Lgak2TSjE0kiV/PVW\nHbwd48DS9ZAU6HEHlSajnCjww/zeOS0HB1uN0ktx5NeVvBxJgE5hmfL8hsaMWSQH\nQ9BvQtR1Aa3EmJXiULlf+SbymwKBgQDSdINlxVJgkQxMcy1m5ryHQOK0lQ3nrqfU\nTs99Zycyy3SB3IKb7RvJkNrP1LgGpmrLRRkSKOSyiaLfinaZbMrwHSRqpj6fPd+0\nDmVWAFFkBLcEfa36UIVj8l42by3vviiDnWFX6bMBpQf3rtssJxD86D73GkTGimN6\n0hMGwDd92QKBgGUoImG90g5BifTYqPod6xtIHRDJw/7x278mBUH28EfTQr8Kj5LQ\nlRQjt1nnJlu4b+AGhTdrEz45TPJGJIeNw3UZ8Jfq0KriJ4wc44+BIHyObP2oB/r6\nrr1ajySFwnryhyyRF8quXshEvFTeBJhaHJV/qo2b+b74CI6yY0rhh++BAoGBALWN\n0CEGtgcaemqlXgCMgw2HW0v/3kwFL0GHb5Y2W+mE/RBeg8oyJaugrDNoxgzumeib\nxoIVBx5CgGCqGaBH61ZzPYxHuHQ1s3uog6FKiuxm6OAV7/WmazyKxN3e0dklyduo\n9GYb9NIxZYs1rFwNo6kaTd9sKPLCRgmZmtJTFkwZAoGBAJ/uhY2YIAyw66XblgAO\n2lZ9LMLujp2m8c53rKZ8/imAFE6imQo0IOFi8v+VWVA/5a76aHbC3w0eZdrdRW82\nKPFdh2qi+BG7bsLtuQzQXgHG0jpl2WWaik3NYbbKCY6I44aGqKITfVrbqPHMGb8N\n/ePvOfaKG0v4l4l5o1+cwpwU\n-----END PRIVATE KEY-----\n",
         "client_email": "firebase-adminsdk-gcg87@almundo-ar.iam.gserviceaccount.com",
         "client_id": "106304437130467727272",
         "auth_uri": "https://accounts.google.com/o/oauth2/auth",
         "token_uri": "https://accounts.google.com/o/oauth2/token",
         "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
         "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-gcg87%40almundo-ar.iam.gserviceaccount.com"
      },
      databaseURL: "https://almundo-ar.firebaseio.com"
   },
   test_env: 'test', // Test Enviroment
   test_db: 'test', // Test Database
   test_port: 3001 // Test Port
}

export default config

