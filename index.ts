// LIBRARIES
import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as cookieParser from 'cookie-parser'
import * as logger from 'morgan'
import * as helmet from 'helmet'
import * as cors from 'cors'


import router from './routes/api'
import config from './config/main'

// Initialize app of express
const app: any = express();

// Express Middlewares
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(logger('dev'));
app.use(helmet())
app.use(cors())
app.use((req, res, next) => {
   res.header('Access-Control-Allow-Origin', '*');
   res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-Width, Content-Type, Accept, Access-Control-Allow-Request-Method');
   res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE')
   res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE')
   next()
})

// Initialize Router
router(app)
 

// Initialize Server
let server: any;
if (process.env.NODE_ENV !== config.test_env) {
   server = app.listen(config.port, () => {
      console.log(`Server Listening on port ${ config.port }`)
   })
} else {
   server = app.listen(config.test_port, () => {
      console.log(`Server Listening on port ${ config.port }`)
   })
}

export default server;